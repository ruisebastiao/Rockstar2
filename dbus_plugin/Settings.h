/*This file is auto generate by pkg.deepin.io/dbus-generator. Don't edit it*/
#include <QtDBus>
QVariant unmarsh(const QVariant&);
QVariant marsh(QDBusArgument target, const QVariant& arg, const QString& sig);
QVariant translateI18n(const char* dir, const char* domain,  const QVariant v);

#ifndef __Settings_H__
#define __Settings_H__

class SettingsProxyer: public QDBusAbstractInterface
{
    Q_OBJECT
public:
    SettingsProxyer(const QString &path, QObject* parent)
          :QDBusAbstractInterface("pub.geekcentral.Rockstar", path, "pub.geekcentral.Rockstar.Settings", QDBusConnection::sessionBus(), parent)
    {
	    if (!isValid()) {
		    qDebug() << "Create Settings remote object failed : " << lastError().message();
	    }
    }
    QVariant fetchProperty(const char* name) {
	QDBusMessage msg = QDBusMessage::createMethodCall(service(), path(),
		QLatin1String("org.freedesktop.DBus.Properties"),
		QLatin1String("Get"));
	msg << interface() << QString::fromUtf8(name);
	QDBusMessage reply = connection().call(msg, QDBus::Block, timeout());
	if (reply.type() != QDBusMessage::ReplyMessage) {
	    qDebug () << QDBusError(reply) << "at " << service() << path() << interface() << name;
	    return QVariant();
	}
	if (reply.signature() != QLatin1String("v")) {
	    QString errmsg = QLatin1String("Invalid signature org.freedesktop.DBus.Propertyies in return from call to ");
	    qDebug () << QDBusError(QDBusError::InvalidSignature, errmsg.arg(reply.signature()));
	    return QVariant();
	}

	QVariant value = qvariant_cast<QDBusVariant>(reply.arguments().at(0)).variant();
	return value;
    }



Q_SIGNALS:
};

class Settings : public QObject 
{
    Q_OBJECT
private:
    QString m_path;
    Q_SLOT void _propertiesChanged(const QDBusMessage &msg) {
	    QList<QVariant> arguments = msg.arguments();
	    if (3 != arguments.count())
	    	return;
	    QString interfaceName = msg.arguments().at(0).toString();
	    if (interfaceName != "pub.geekcentral.Rockstar.Settings")
		    return;

	    QVariantMap changedProps = qdbus_cast<QVariantMap>(arguments.at(1).value<QDBusArgument>());
	    foreach(const QString &prop, changedProps.keys()) {
		    if (0) { 
		    }
	    }
    }
    void _rebuild() 
    { 
	  delete m_ifc;
          m_ifc = new SettingsProxyer(m_path, this);
	  _setupSignalHandle();
    }
    void _setupSignalHandle() {

    }
public:
    Q_PROPERTY(QString path READ path WRITE setPath NOTIFY pathChanged)
    const QString path() {
	    return m_path;
    }
    void setPath(const QString& path) {
	    QDBusConnection::sessionBus().disconnect("pub.geekcentral.Rockstar", m_path, "org.freedesktop.DBus.Properties", "PropertiesChanged",
	    				 this, SLOT(_propertiesChanged(QDBusMessage)));
	    m_path = path;
	    QDBusConnection::sessionBus().connect("pub.geekcentral.Rockstar", m_path, "org.freedesktop.DBus.Properties", "PropertiesChanged",
	    				"sa{sv}as", this, SLOT(_propertiesChanged(QDBusMessage)));
	    _rebuild();
    }
    Q_SIGNAL void pathChanged(QString);

    Settings(QObject *parent=0) : QObject(parent), m_ifc(new SettingsProxyer("/pub/geekcentral/Rockstar/Settings", this))
    {
	    _setupSignalHandle();
	    QDBusConnection::sessionBus().connect("pub.geekcentral.Rockstar", m_path, "org.freedesktop.DBus.Properties", "PropertiesChanged",
	    				"sa{sv}as", this, SLOT(_propertiesChanged(QDBusMessage)));
    }
    

    //Property read methods
    //Property set methods :TODO check access

public Q_SLOTS:  
    QVariant value(const QVariant &key) {
	    QList<QVariant> argumentList;
	    argumentList << marsh(QDBusArgument(), key, "s");

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("value"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at pub.geekcentral.Rockstar.Settings.value:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    if (args.size() != 1) {
		    qDebug() << "Warning: \"pub.geekcentral.Rockstar.Settings.value\" excepted one output parameter, but got " << args.size();
		    return QVariant();
	    }
	    return unmarsh(args[0]);
	    
    }
  
    QVariant setValue(const QVariant &arg0, const QVariant &arg1) {
	    QList<QVariant> argumentList;
	    argumentList << marsh(QDBusArgument(), arg0, "s") << marsh(QDBusArgument(), arg1, "v");

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("setValue"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at pub.geekcentral.Rockstar.Settings.setValue:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    return QVariant();
	    
    }


Q_SIGNALS:
//Property changed notify signal

//DBus Interface's signal
private:
    SettingsProxyer *m_ifc;
};

#endif
