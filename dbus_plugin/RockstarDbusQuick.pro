TEMPLATE = lib
TARGET = Rockstar
QT += qml quick dbus
CONFIG += qt plugin c++11

TARGET = $$qtLibraryTarget($$TARGET)
uri = Rockstar.DBus

# dbus-generator -in=dbus_plugins.json -out="." -target="qml"
HEADERS += plugin.h \
           Settings.h \
           Weather.h \
           RssArticle.h \
           Rss.h \
           Mpris.h \
           Player.h \
           Playlists.h \
           UPower.h \
           UPowerDevice.h \
           ObjectManager.h \
           UDisks2Manager.h \
           UDisks2Drive.h \
           UDisks2Block.h \
           UDisks2Filesystem.h \
           Alexa.h

DESTDIR = $$replace(uri, \\., /)

DISTFILES = dbus_plugins.json qmldir

!equals(_PRO_FILE_PWD_, $$OUT_PWD) {
    copy_qmldir.target = $$OUT_PWD/$$replace(uri, \\., /)/qmldir
    copy_qmldir.depends = $$_PRO_FILE_PWD_/qmldir
    copy_qmldir.commands = $(COPY_FILE) \"$$replace(copy_qmldir.depends, /, $$QMAKE_DIR_SEP)\" \"$$replace(copy_qmldir.target, /, $$QMAKE_DIR_SEP)\"
    QMAKE_EXTRA_TARGETS += copy_qmldir
    PRE_TARGETDEPS += $$copy_qmldir.target
}

qmldir.files = qmldir
unix {
    installPath = $$[QT_INSTALL_QML]/$$replace(uri, \\., /)
    qmldir.path = $$installPath
    target.path = $$installPath
    INSTALLS += target qmldir
}

